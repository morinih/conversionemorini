import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import static java.lang.Long.parseLong;

public class Main {

    public static void main(String[] args) {

        System.out.println("Inserisci la valuta (USD, INR): ");
        Scanner scanner = new Scanner(System.in);
        String base = scanner.nextLine();

        System.out.println("Inserisci l'importo: ");
        //Scanner scanner2 = new Scanner(System.in);
        String importo = scanner.nextLine();

        GetFixerData data = new GetFixerData("EUR", base);
        try{
            String json = data.getData();

            if(json.isEmpty())
                throw new Exception();
            System.out.println(json);

            JsonElement element = new JsonParser().parse(json);
            JsonObject object = element.getAsJsonObject();

            JsonObject joRates = object.getAsJsonObject("rates");
            String sBase = joRates.get(base).getAsString();

            Double valuta = Double.parseDouble(importo)/Double.parseDouble(sBase);

            System.out.println("Risultato = " + valuta);

            String TimeStamp = object.get("timestamp").getAsString();

            //Date date=new Date(parseLong(TimeStamp));
            String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date (parseLong(TimeStamp)*1000));
            System.out.println(date);

        }catch(Exception e){
            System.out.println("Errore invocazione web-service!");
        }


    }


}
