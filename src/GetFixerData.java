public class GetFixerData extends HttpObject{
    private String api_key = "648ce0e08a14c5bd984b85314662567b";
    private String json_host = "https://fixer.io/";
    private String json_prefix = "http://data.fixer.io/api/latest?";
    private String url;
    String address;
    private String json_data;

    public GetFixerData(String base, String symbols){
        url = json_prefix +"&access_key=" + api_key + "&base=" + base + "&symbols=" + symbols;
        setHost(json_host);
        setUrl(url);
        setCharset("UTF-8");
        setMethod("GET");
        setType("application/json");
    }

    @Override
    public String getData(){
        super.run();
        return super.getData();
    }
}