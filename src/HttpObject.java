import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Classe per utilizzare i socket
 */
public class HttpObject {
    /**
     * Insieme dei dati restituiti dal webservice
     */
    private String data;
    /**
     * Indirizzo IP dell'host di appartenenza webservice
     */
    private String host;
    /**
     * Querystring di richiesta verso il webservice
     */
    private String url;
    /**
     * Set di caratteri utilizzati nella transazione
     */
    private String charset;
    /**
     * Metodo utilizzato (GET, POST, DELETE, UPDATE)
     */
    private String method;
    /**
     * Tipo di informazione attesa dal webservice (text, json, ...)
     */
    private String type;

    public String getData() {
        return data;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private URL server;
    private HttpURLConnection service;
    private BufferedReader input;
    private int status;
    private String line;

    /**
     * Costruttore di default
     */
    public HttpObject(){
        this.host = "";
        this.url = "";
        this.charset = "UTF-8";
        this.method = "GET";
        this.type = "application/json";
    }

    /**
     * Costruttore
     * <
     * @param host
     * @param url
     * @param charset
     * @param method
     * @param type
     */
    public HttpObject(String host, String url, String charset, String method, String type){
        this.host = host;
        this.url = url;
        this.charset = charset;
        this.method = method;
        this.type = type;
    }

    /**
     * Costruttore
     *
     * @param host
     * @param url
     */
    public HttpObject(String host, String url){
        this.host = host;
        this.url = url;
        this.charset = "UTF-8";
        this.method = "GET";
        this.type = "application/json";
    }

    public boolean run(){
        try{
            //Costruzione dell'URL di interrogazione webservice
            server = new URL(url);
            service = (HttpURLConnection)server.openConnection();
            //Impostazione header richiesta
            service.setRequestProperty("Host", host);
            service.setRequestProperty("Accept", type);
            service.setRequestProperty("Accept-Charset", charset);
            //Impostazione metodo di richiesta
            service.setRequestMethod(method);
            //Attivazione ricezione
            service.setDoInput(true);
            //Connessione al web-service
            service.connect();
            //Verifica stato risposta
            status = service.getResponseCode();
            if(status != 200){
                return false;
            }
            //Apertura stream di ricezione da risorsa web
            input = new BufferedReader(new InputStreamReader(service.getInputStream(), charset));
            //Ciclo di lettura da web
            data = "";
            while((line = input.readLine()) != null) {
                data += line;
            }
            input.close();
            return true;
        }catch(IOException e){
            return false;
        }
    }
}